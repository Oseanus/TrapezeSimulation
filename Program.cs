﻿/***********************************************
*                                              *
*   Simulation Engineering                     *
*   Homework 2: Adding Monte Carlo Simulation  *
*                                              *
*   By Oliver Ellis: 491848                    *
*                                              *
************************************************/

using System;
using System.Collections.Generic;
using TrapezeSimulation.Simulation;
using TrapezeSimulation.Data;

namespace TrapezeSimulation
{
    class Program
    {
        static void Main(string[] args)
        {
             // Set data set for the simulation
            var dataSet = new PendulumDataSet
            {
                GravitationalAcceleration = 9.81,
                StepTime = 0.01,
                EndTime = 10.0,
            };

            // Initialise simulator
            TrapezeSimulator simulator = new TrapezeSimulator(dataSet);

            // Execute the Monte Carlo simulation with 1000 iterations
            for(int i = 0; i < 1000; i++)
            {               
                simulator.Initialise();
            }

            // Write results of the Monte Carlo simulation to csv file
            simulator.WritePeriodsToCSV();
        }
    }
}
