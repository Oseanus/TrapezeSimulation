using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using TrapezeSimulation.Data;

namespace TrapezeSimulation.Simulation
{
    public class TrapezeSimulator
    {
        #region fields
        private PendulumDataSet _dataSet;
        private List<StateVector> _listStateVectors;
        private List<double> _periods;
        private int _counter;
        private double _lastTime;
        private double _deltaTime;
        #endregion

        #region ctor
        public TrapezeSimulator(PendulumDataSet dataSet)
        {
            _counter = 0;
            _lastTime = 0;
            _deltaTime = 0;
            _dataSet = dataSet;
            _periods = new List<double>();          
            _listStateVectors = new List<StateVector>();
        }
        #endregion

        #region public methods
        public void Initialise()
        {
            _dataSet.Length = ComputeRandomLength();
            _dataSet.InitialAngle = ComputeInitialAngle();

            StateList states = new StateList(_dataSet.StepTime);
            
            State angle = new State((Math.PI / 180) * _dataSet.InitialAngle); // Theta1
            double previousAngle = 0;

            double initialVelocity = ComputeInitialVelocity();
            State velocity = new State(initialVelocity); // Theta2

            states.Add(angle);
            states.Add(velocity);

            while(states.Time <= _dataSet.EndTime)
            {
                // Fill column list
                _listStateVectors.Add(new StateVector
                {
                    Time = states.Time,
                    Angle = Math.Round((angle.Status * 180) / Math.PI, 5),
                    Velocity = Math.Round(velocity.Status, 10)
                });

                // Determine new state
                double sinus = Math.Sin(angle.Status);
                velocity.Derivate = -(_dataSet.GravitationalAcceleration / _dataSet.Length) * sinus;
                previousAngle = angle.Status;
                angle.Derivate = velocity.Status;

                states.Integrate();

                CountPeriods(angle.Status, previousAngle, states.Time);

                // Reset values before next iteration
                _dataSet.Length = ComputeRandomLength();
            }

            Reset();
        }

        public void DisplayResults()
        {
            Console.WriteLine("Time | Status | Velocity");

            foreach(StateVector s in _listStateVectors)
            {
                Console.WriteLine($"{s.Time} | {s.Angle} | {s.Velocity}");
            }
        }

        // Writes the results of one iteration to a CSV-file.
        // Warning! Do not use this method while Monte Carlo is implemented!
        public void WriteToCSV()
        {
            StringBuilder csvContent = new StringBuilder();

            // Append first line
            csvContent.AppendLine("Time;Angle;Velocity");

            foreach(StateVector s in _listStateVectors)
            {
                string line = String.Format($"{s.Time};{s.Angle};{s.Velocity}");
                csvContent.AppendLine(line);
            }

            string path = "simulation.csv";
            File.AppendAllText(path, csvContent.ToString());
        }

        // Writes all periods counted to a CSV-file.
        public void WritePeriodsToCSV()
        {
            StringBuilder csvContent = new StringBuilder();

            int i = 1;
            foreach(double p in _periods)
            {
                if(p > 0)
                {
                    string line = String.Format($"{i};{p}");
                    csvContent.AppendLine(line);
                    i++;
                }
            }

            string path = "monte_carlo.csv";
            File.AppendAllText(path, csvContent.ToString());
        }

        public void DisplayPeriods()
        {
            int i = 1;

            foreach(double p in _periods)
            {
                Console.WriteLine($"{i}. {p}");

                i++;
            }
        }
        #endregion

        #region private methods
        private double ComputeInitialAngle()
        {
            return 55 + 10 * new Random().NextDouble();
        }

        private double ComputeRandomLength()
        {
            return 9.5 + new Random().NextDouble();
        }

        private double ComputeInitialVelocity()
        {
            return 0.25 + 0.1 * new Random().NextDouble();
        }

        private void CountPeriods(double angle, double previousAngle, double time)
        {
            if(angle * previousAngle <= 0)
            {
                _counter++;

                // Initial assignment to _lastTime and return afterwards
                if(_lastTime == 0)
                {
                    _lastTime = time;
                    return;
                }

                _deltaTime = time - _lastTime;
            }
            
            if(_counter == 3)
            {
                _periods.Add(_deltaTime);
                _lastTime = time;

                _counter = 0;
            }
        }

        private void Reset()
        {
            _lastTime = 0;
            _deltaTime = 0;
        }
        #endregion
    }
}