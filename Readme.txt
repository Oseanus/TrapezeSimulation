This programme is written in C#.
Author: Oliver Ellis, 491848

1. Design
It contains 5 classes and 1 Main class:
- Program.cs --> Main class
- Simulation/TrapezeSimulation.cs
- Data/PendulumDataSet.cs
- Data/State.cs
- Data/StateList.cs
- Data/StateVector.cs

2. Running this programme
It is implemented in C# via the .Net Core 2 framework. Therefore it is required to have this installed.
If Visual Studio 2017 is installed it may work as well.
This programme is a console application and generates a CSV-file in which all counted periods are listed.
Each column is seperated with ";".
To execute the application please be sure that the file monte_carlo.csv is deleted.