namespace TrapezeSimulation.Data
{
    public class PendulumDataSet
    {
        private double _initialAngle;
        private double _gravitationalAcceleration; 
        private double _stepTime;
        private double _endTime;
        private double _length;

        public double InitialAngle
        {
            get { return _initialAngle; }
            set { _initialAngle = value; }
        }

        public double GravitationalAcceleration
        {
            get { return _gravitationalAcceleration; }
            set { _gravitationalAcceleration = value ;}
        }

        public double StepTime
        {
            get { return _stepTime; }
            set { _stepTime = value; }
        }

        public double EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        public double Length
        {
            get { return _length; }
            set { _length = value; }
        }
    }
}