namespace TrapezeSimulation.Data
{
    public class State
    {
        #region fields
        private double _state;
        private double _previousState;
        private double _derivate;
        private double[] _previousDerivate;

        public double Status
        {
            get { return _state; }
            set { _state = value; }
        }

        public double PreviousState
        {
            get { return _previousState; }
            set { _previousState = value; }
        }

        public double Derivate
        {
            get { return _derivate; }
            set { _derivate = value; }
        }

        public double[] PreviousDerivate
        {
            get { return _previousDerivate; }
            set { _previousDerivate = value; }
        }
        #endregion

        #region ctor
        public State(double initialState)
        {
            _state = initialState;
            _previousState = 0;
            _previousDerivate = new double[] {0, 0, 0};
            _derivate = 0;
        }
        #endregion

        #region methods
        public void NextStep(int frameCount, double stepTime)
        {
            switch(frameCount & 3)
            {
                case 0:
                    _previousState = _state;
                    _state = _state + 2 * stepTime * _derivate;
                    _previousDerivate[2] = _derivate;
                    break;
                case 1:
                    _state = _previousState + 2 * stepTime * _derivate;
                    _previousDerivate[1] = _derivate;
                    break;
                case 2:
                    _state = _previousState + 4 * stepTime * _derivate;
                    _previousDerivate[0] = _derivate;
                    break;
                case 3:
                    _state = _previousState + (stepTime / 6) *
                                (4 * _derivate + 
                                 8 * _previousDerivate[0] +
                                 8 * _previousDerivate[1] +
                                 4 * _previousDerivate[2]);
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}