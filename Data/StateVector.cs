namespace TrapezeSimulation.Data
{
    public class StateVector
    {
        #region fields
        public double Time { get; set; }
        public double Angle { get; set; }
        public double Velocity { get; set; }
        #endregion

        #region ctor
        public StateVector()
        {
            
        }
        #endregion
    }
}