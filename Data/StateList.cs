using System;
using System.Collections.Generic;

namespace TrapezeSimulation.Data
{
    public class StateList
    {
        #region fields
        private int _frameCount;
        private double _stepTime;
        private double _time;
        private List<State> _states;

        public double Time 
        {
             get { return _time; }
        }

        public int FrameCount
        {
            get { return _frameCount; }
        }

        public double StepTime
        {
            get { return _stepTime; }
        }

        public double DoubleStepTime
        {
            get { return _stepTime * 2; }
        }

        public double HalfStepTime
        {
            get { return _stepTime / 2; }
        }

        public List<State> States
        {
            get { return _states; }
        }
        #endregion

        #region ctor
        public StateList(double stepTime)
        {
            _frameCount = 0;
            _stepTime = stepTime;
            _time = 0;
            _states = new List<State>();
        }
        #endregion

        #region methods
        public void Add(State state)
        {
            _states.Add(state);
        }

        public void Integrate()
        {
            foreach(State state in _states)
            {
                state.NextStep(_frameCount, _stepTime);
            }

            _frameCount++;
            
            _time = Math.Round(_time + _stepTime, 2);
        }
        #endregion
    }
}